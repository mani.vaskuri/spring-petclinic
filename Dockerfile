FROM openjdk:19-jdk-alpine
ARG JAR_FILE=deployment_directory/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["java","-jar","/app.jar"]